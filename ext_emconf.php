<?php
$EM_CONF[$_EXTKEY] = array(
	'title' => 'Margins Migrate',
	'description' => "Migrates old style margins to new class style margins.",
	'category' => 'be',
	'version' => '0.1.3',
	'state' => 'beta',
	'author' => 'Stig Nørgaard Færch',
	'author_email' => 'snf@dkm.dk',
	'constraints' => array(
		'depends' => array(
			'typo3' => '9.5-10.4.99',
		),
		'conflicts' => array(
		),
		'suggests' => array(
		),
	),
	'_md5_values_when_last_written' => 'a:10:{s:9:"ChangeLog";s:4:"591d";s:10:"README.txt";s:4:"9fa9";s:45:"class.tcamanipulate_tceforms_procTCAtitle.php";s:4:"96db";s:44:"class.tcamanipulate_userauth_TCApostproc.php";s:4:"f1eb";s:12:"ext_icon.gif";s:4:"c0ed";s:17:"ext_localconf.php";s:4:"d687";s:14:"ext_tables.php";s:4:"8fdb";s:14:"doc/manual.sxw";s:4:"1af7";s:19:"doc/wizard_form.dat";s:4:"9d7d";s:20:"doc/wizard_form.html";s:4:"ad55";}',
);
