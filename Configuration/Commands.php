<?php
return [
    'margins:migrate' => [
        'class' => \DKM\MarginsMigrate\Command\MigrateMarginsCommand::class,
        'schedulable' => true,
    ],
    'margins:reset' => [
        'class' => \DKM\MarginsMigrate\Command\ResetMarginsCommand::class,
        'schedulable' => true,
    ]
];